## JS Random

Javascript Version der PHP rand() Funktion

function rand(min, max) {  
    return Math.floor(Math.random() * (max - min + 1)) + min;  
}

// Beispiel: random number between 1 und 10

let min = 1
let max = 10
console.log( random(min, max) )
